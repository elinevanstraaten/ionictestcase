import {Component, inject} from '@angular/core';
import {File} from '@ionic-native/file/ngx';
import {Events, ModalController, NavController, PopoverController, ToastController} from "@ionic/angular";
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import {async} from "@angular/core/testing";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  private insertstring;
  private blob: Blob;

  constructor(private nav: NavController, private  file: File, public toastController: ToastController) {
  }

  all = "";
  boxes = [];
  boxarray = [];
  total=0;


  newBox() {
    this.boxarray.push(1);

  }

  saveValues() {
    for (let i = 0; i < this.boxes.length; i++) {
      console.log(this.boxes[i]);
      this.total += this.boxes[i];
    }
    this.all = this.boxes.toString()
    console.log(this.total);
    this.GetFile();
    this.nav.navigateForward('/chart/' + this.all);

  }

  GetFile() {

    this.file.createFile(this.file.externalRootDirectory + '/Download/', "Piechartvalues.txt", true);
    this.insertstring = "\n\n Total:" + this.total.toString() + "\nValues: " + this.boxes.toString();
    this.blob = new Blob([this.insertstring], {type: 'text/plain'});
    this.file.writeFile(this.file.externalRootDirectory + '/Download/', "Piechartvalues.txt", this.blob, {replace: false,append: true});
    this.presentToast();
  }


  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Your values have been saved in "Downloads."',
      duration: 2000
    });
    toast.present();
  }
}


